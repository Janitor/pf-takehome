from pfapp.views._base import BaseCollection, BaseUnit, pagination_arguments
from pfapp.restwrapper import api
from pfapp.serializer import item, page_items
from pfapp.database.models import Item
from pfapp.database.functions.crud_functions import genfunctions

ns = api.namespace('items', description='Operations related to items')


@ns.route('/')
class ItemCollection(BaseCollection):
    model = Item
    functions = genfunctions(Item)

    @api.expect(pagination_arguments)
    @api.marshal_with(page_items)
    def get(self):
        """
        Returns a paginated list of all items.
        """
        return super().get()

    @api.response(201, 'Item successfully created.')
    @api.expect(item)
    def post(self):
        """
        Creates a new item.
        """
        return super().post()


@ns.route('/<int:id>')
@api.response(404, 'Item not found.')
class ItemUnit(BaseUnit):
    model = Item
    functions = genfunctions(Item)

    @api.marshal_list_with(item)
    def get(self, id):
        """
        Returns an item's information.
        """
        return super().get(id)

    @api.response(204, 'Item successfully updated.')
    @api.expect(item)
    def put(self, id):
        """
        Updates an item.
        """
        return super().put(id)

    @api.response(204, 'Item successfully deleted.')
    def delete(self, id):
        """
        Deletes an item.
        """
        return super().delete(id)
