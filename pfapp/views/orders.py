from flask import request
from pfapp.views._base import BaseCollection, BaseUnit, pagination_arguments
from pfapp.restwrapper import api
from pfapp.serializer import order, page_orders, item, item_id
from pfapp.database.models import Order
from pfapp.database.functions.order_functions import OrderFunctions

ns = api.namespace('orders', description='Operations related to orders')


@ns.route('/')
class OrderCollection(BaseCollection):
    model = Order
    functions = OrderFunctions()

    @api.expect(pagination_arguments)
    @api.marshal_with(page_orders)
    def get(self):
        """
        Returns a paginated list of all orders.
        """
        return super().get()

    @api.expect(order, validate=True)
    @api.response(201, 'Order successfully created.')
    def post(self):
        """
        Creates a new order.
        """
        return super().post()


@ns.route('/<int:id>')
@api.response(404, 'Order not found.')
class OrderUnit(BaseUnit):
    model = Order
    functions = OrderFunctions()

    @api.marshal_list_with(order)
    def get(self, id):
        """
        Returns an order's information.
        """
        return super().get(id)

    @api.expect(order)
    @api.response(204, 'Order successfully updated.')
    def put(self, id):
        """
        Updates an order.
        """
        return super().put(id)

    @api.response(204, 'Order successfully deleted.')
    def delete(self, id):
        """
        Deletes an order.
        """
        return super().delete(id)


@ns.route('/<int:id>/items')
@api.response(404, 'Order not found.')
class OrderItemUnit(BaseUnit):
    model = Order
    functions = OrderFunctions()

    @api.marshal_list_with(item)
    def get(self, id):
        """
        Returns a list of items on an order.
        I wanted to demonstrate other get functionality so I decided to not paginate this result.
        This seemed like the best place to fallback to a standard model because paging through an order/cart as
        a customer would not be a good experience.
        """
        return self.functions.get_items(id)

    @api.response(204, 'Item successfully added.')
    @api.expect(item_id)
    def put(self, id):
        """
        Adds an item to an order.
        """
        data = request.json
        self.functions.add_item(id, data)
        return None, 204

    @api.response(204, 'Item successfully removed.')
    @api.expect(item_id)
    def delete(self, id):
        """
        Removes an item on an order.
        """
        data = request.json
        self.functions.remove_item(id, data)
        return None, 204
