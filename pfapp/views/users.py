from pfapp.views._base import BaseCollection, BaseUnit, pagination_arguments
from pfapp.restwrapper import api
from pfapp.serializer import user, page_users
from pfapp.database.models import User
from pfapp.database.functions.crud_functions import genfunctions

ns = api.namespace('users', description='Operations related to users')


@ns.route('/')
class UserCollection(BaseCollection):
    model = User
    functions = genfunctions(User)

    @api.expect(pagination_arguments)
    @api.marshal_with(page_users)
    def get(self):
        """
        Returns a paginated list of all users.
        """
        return super().get()

    @api.response(201, 'User successfully created.')
    @api.expect(user)
    def post(self):
        """
        Creates a new user.
        """
        return super().post()


@ns.route('/<int:id>')
@api.response(404, 'User not found.')
class UserUnit(BaseUnit):
    model = User
    functions = genfunctions(User)

    @api.marshal_list_with(user)
    def get(self, id):
        """
        Returns a user's information.
        """
        return super().get(id)

    @api.response(204, 'User successfully updated.')
    @api.expect(user)
    def put(self, id):
        """
        Updates a user.
        """
        return super().put(id)

    @api.response(204, 'User successfully deleted.')
    def delete(self, id):
        """
        Deletes a user.
        """
        return super().delete(id)
