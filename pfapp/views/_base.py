from flask import request
from flask_restplus import Resource, reqparse
from pfapp.database import db
from pfapp.database.functions.crud_functions import genfunctions

pagination_arguments = reqparse.RequestParser()
pagination_arguments.add_argument('page', type=int, required=False, default=1, help='Page number')
pagination_arguments.add_argument('per_page', type=int, required=False, choices=[10, 20, 30, 40, 50],
                                  default=10, help='Results per page')


class BaseCollection(Resource):
    model = db.Model
    functions = genfunctions(db.Model)

    def get(self):
        """
        Returns a paginated list of units.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)
        base_query = self.model.query
        base_page = base_query.paginate(page, per_page, error_out=False)
        return base_page

    def post(self):
        """
        Creates a new unit.
        """
        data = request.json
        self.functions.create(data)
        return None, 201


class BaseUnit(Resource):
    model = db.Model
    functions = genfunctions(db.Model)

    def get(self, id):
        return self.model.query.filter(self.model.id == id).one()

    def put(self, id):
        data = request.json
        self.functions.update(id, data)
        return None, 204

    def delete(self, id):
        self.functions.delete(id)
        return None, 204
