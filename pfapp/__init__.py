import os
import sys
from pfapp.database import db
from pfapp.config import CONFIG_NAME_MAPPER
from pfapp.restwrapper import api
from pfapp.views.users import ns as users_namespace
from pfapp.views.items import ns as items_namespace
from pfapp.views.orders import ns as orders_namespace
from flask import Flask, Blueprint


def create_app(flask_config=None, **kwargs):
    """
    The instantiation function for pfapp.
    :param flask_config: (optional) Raw flask config dictionary
    :param kwargs: kwargs for Flask instantiation.
    :return: Flask app configured with namespaces.
    """
    flask_app = Flask(__name__, **kwargs)
    if not flask_config:
        config_name = os.getenv('FLASK_CONFIG')
        try:
            flask_app.config.from_object(CONFIG_NAME_MAPPER[config_name])
        except ImportError:
            flask_app.logger.error(
                "Please set `FLASK_CONFIG` environment variable to one of the following ",
                "options: development, production, testing"
            )
            sys.exit(1)
    else:
        flask_app.config.update(flask_config)
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(users_namespace)
    api.add_namespace(items_namespace)
    api.add_namespace(orders_namespace)
    flask_app.register_blueprint(blueprint)
    db.init_app(flask_app)
    return flask_app
