import logging
import traceback

from flask_restplus import Api
from pfapp.config import Config
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from pfapp.database.exceptions import ForeignKeyException

log = logging.getLogger(__name__)

api = Api(version='1.0', title='Takehome API',
          description='Developing an API against a given SQLite schema.')


@api.errorhandler
def default_error_handler(e):
    """
    Default error handler
    :param e: Unhandled Error
    :return: Error 500 and generic message
    """
    message = 'An unhandled exception occurred.'
    log.exception(message)

    if not Config.DEBUG:
        return {'message': message}, 500


@api.errorhandler(NoResultFound)
def no_results(e):
    """
    SQL NoResultFound error handler.
    :param e:  NoResultFound error
    :return: Error 404 and generic message
    """
    log.warning(traceback.format_exc())
    return {'message': 'No results matching criteria.'}, 404


@api.errorhandler(IntegrityError)
def constraint_fail(e):
    """
    SQL Integrity Error error handler.
    :param e: IntegrityError
    :return: Error 404 and generic data violation message.
    """
    log.warning(traceback.format_exc())
    return {'message': 'Data violates a table constraint.'}, 404


@api.errorhandler(ForeignKeyException)
def foreign_key(e):
    """
    SQL ForeignKeyException
    :param e: ForeignKeyException
    :return: Error 404 and generic foreign key constraint error message.
    """
    log.warning(traceback.format_exc())
    return {'message': 'FK Constraint: %s in %s not met.' % (e.key, e.table)}, 404
