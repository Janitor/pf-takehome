from flask_restplus import fields
from pfapp.restwrapper import api

pagination = api.model('A page of units', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

user = api.model('User', {
    'id': fields.Integer(readonly=True, description='The unique identifier of a user'),
    'name': fields.String(required=True, description='User name'),
})

page_users = api.inherit('Page of users', pagination, {
    'items': fields.List(fields.Nested(user))
})

item = api.model('Item', {
    'id': fields.Integer(readonly=True, description='The unique identifier of an item'),
    'name': fields.String(required=True, description='Item name'),
})

item_id = api.model('Item2', {
    'item_id': fields.Integer(required=True, description='The unique identifier of an item')
})

page_items = api.inherit('Page of items', pagination, {
    'items': fields.List(fields.Nested(item))
})

order = api.model('Order', {
    'id': fields.Integer(readonly=True, description='The unique identifier of an order'),
    'user_id': fields.Integer(attribute='user.id', required=True, description='User Id assigned to order'),
})

page_orders = api.inherit('Page of orders', pagination, {
    'items': fields.List(fields.Nested(order))
})

page_order_items = api.inherit('Page of items on order', pagination, {
    'items': fields.List(fields.Nested(item))
})
