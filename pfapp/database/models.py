from pfapp.database import db


class Item(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    def __init__(self, name):
        self.name = name

    orders = db.relationship('Order', secondary='order_items', backref='items')


order_items = db.Table(
    'order_items',
    db.Column('order_id', db.ForeignKey('orders.id'), primary_key=True, nullable=False),
    db.Column('item_id', db.ForeignKey('items.id'), primary_key=True, nullable=False)
)


class Order(db.Model):
    __tablename__ = 'orders'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey('users.id'), nullable=False)

    user = db.relationship('User', primaryjoin='Order.user_id == User.id', backref='orders')
    order_items = db.relationship('Item', secondary=order_items, backref="_orders")

    def __init__(self, user):
        self.user = user


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    def __init__(self, name):
        self.name = name
