class ForeignKeyException(Exception):
    def __init__(self, key, table):
        self.key = key
        self.table = table
