from pfapp.database import db
from pfapp.database.functions.crud_functions import CrudFunctions
from pfapp.database.models import User, Order, Item
from pfapp.database.exceptions import ForeignKeyException
from sqlalchemy.orm.exc import NoResultFound


class OrderFunctions(CrudFunctions):

    def __init__(self):
        self.model = Order

    def create(self, data):
        user_id = data.get('user_id')
        try:
            user = User.query.filter(User.id == user_id).one()
        except NoResultFound as e:
            raise ForeignKeyException("user_id", "orders")
        order = self.model(user)
        db.session.add(order)
        db.session.commit()

    def update(self, order_id, data):
        order = Order.query.filter(Order.id == order_id).one()
        user_id = data.get('user_id')
        try:
            user = User.query.filter(User.id == user_id).one()
        except NoResultFound as e:
            raise ForeignKeyException("user_id", "orders")
        order.user = user
        db.session.add(order)
        db.session.commit()

    def get_items(self, order_id):
        order = Order.query.filter(Order.id == order_id).one()
        return order.order_items

    def add_item(self, order_id, data):
        order = Order.query.filter(Order.id == order_id).one()
        item_id = data.get('item_id')
        try:
            item = Item.query.filter(Item.id == item_id).one()
        except NoResultFound as e:
            raise ForeignKeyException("item_id", "orders")
        order.order_items.append(item)
        db.session.commit()

    def remove_item(self, order_id, data):
        order = Order.query.filter(Order.id == order_id).one()
        item_id = data.get('item_id')
        try:
            item = Item.query.filter(Item.id == item_id).one()
        except NoResultFound as e:
            raise ForeignKeyException("item_id", "orders")
        order.order_items.remove(item)
        db.session.commit()
