from pfapp.database import db


class CrudFunctions:

    def __init__(self, model):
        self.model = model

    def create(self, data):
        unit = self.model(**data)
        db.session.add(unit)
        db.session.commit()

    def delete(self, unit_id):
        unit = self.model.query.filter(self.model.id == unit_id).one()
        db.session.delete(unit)
        db.session.commit()

    def update(self, unit_id, data):
        unit = self.model.query.filter(self.model.id == unit_id).one()
        for key in data:
            setattr(unit, key, data[key])
        db.session.add(unit)
        db.session.commit()


def genfunctions(model):
    return CrudFunctions(model)
