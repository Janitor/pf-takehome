import os


class Config(object):
    """
    Base Flask config object
    """
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = False
    try:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URI']
    except KeyError:
        SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SWAGGER_UI_DOC_EXPANSION = 'list'
    VALIDATE = True
    MASK_SWAGGER = False
    ERROR_404_HELP = False


class ProductionConfig(Config):
    """
    Production Config
    """
    pass


class DevelopmentConfig(Config):
    """
    Development Config
    """
    DEBUG = True
    DEVELOPMENT = True


class TestingConfig(Config):
    """
    Testing Config
    """
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


CONFIG_NAME_MAPPER = {
    'development': 'pfapp.config.DevelopmentConfig',
    'production': 'pfapp.config.ProductionConfig',
    'testing': 'pfapp.config.TestingConfig'
}