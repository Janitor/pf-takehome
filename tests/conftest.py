import pytest
import os
import tempfile
import shutil
from pfapp import create_app

temp_path = os.path.join(tempfile.gettempdir(), 'testing.db')
shutil.copy2('tests/testing.db', temp_path)


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///%s' % temp_path
    })
    yield app


@pytest.fixture
def client(app):
    return app.test_client()
