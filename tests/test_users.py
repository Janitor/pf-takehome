def test_get_users(client, app):
    response = client.get('/api/users/')
    assert int(response.json['total']) > 0
    assert response.json['items']


def test_post_user(client, app):
    mock_user = dict(name="Ronalds Blarg")
    response = client.post('/api/users/', json=mock_user)
    assert response.status_code == 201
    response = client.get('/api/users/')
    assert any(resp['name'] == 'Ronalds Blarg' for resp in response.json['items'])


def test_get_user_id(client, app):
    response = client.get('/api/users/1')
    assert response.status_code == 200
    assert response.json['name'] == 'Charlotte'


def test_put_user(client, app):
    init = client.get('/api/users/4').json

    mock_user = dict(name="New Name")
    response = client.put('/api/users/4', json=mock_user)
    assert response.status_code == 204

    new = client.get('/api/users/4').json
    assert init['id'] == new['id']
    assert init['name'] != new['name'] and new['name'] == 'New Name'


def test_delete_user(client, app):
    response = client.delete('/api/users/4')
    assert response.status_code == 204

    response = client.get('/api/users/4')
    assert response.status_code == 404 and \
        'No results matching criteria.' in response.json['message']

