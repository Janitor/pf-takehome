def test_get_orders(client, app):
    """
    Test get paginated Orders
    """
    response = client.get('/api/orders/')
    assert int(response.json['total']) > 0
    assert response.json['items']


def test_post_order(client, app):
    """
    Test posting an order (userid)
    """
    mock_order = dict(user_id=1)
    response = client.post('/api/orders/', json=mock_order)
    assert response.status_code == 201
    response = client.get('/api/orders/')
    assert any(resp['user_id'] == 1 for resp in response.json['items'])


def test_get_order_id(client, app):
    """
    Test get order by order_id
    """
    response = client.get('/api/orders/2')
    assert response.status_code == 200
    assert response.json['user_id'] == 2


def test_put_order(client, app):
    """
    Test update order's user_id
    """
    init = client.get('/api/orders/4').json

    mock_order = dict(user_id=1)
    response = client.put('/api/orders/4', json=mock_order)
    assert response.status_code == 204

    new = client.get('/api/orders/4').json
    assert init['id'] == new['id']
    assert init['user_id'] != new['user_id'] and new['user_id'] == 1


def test_delete_order(client, app):
    """
    Test delete Order
    """
    response = client.delete('/api/orders/4')
    assert response.status_code == 204

    response = client.get('/api/orders/4')
    assert response.status_code == 404 and \
        'No results matching criteria.' in response.json['message']


def test_get_order_items(client, app):
    """
    Test get order items
    """
    response = client.get('/api/orders/1/items')
    assert len(response.json) > 0
    assert 'id' in response.json[0] and 'name' in response.json[0]


def test_put_order_items(client, app):
    """
    Test update order items
    """
    init = client.get('/api/orders/1/items')
    mock_item = dict(item_id=3)

    response = client.put('/api/orders/1/items', json=mock_item)
    assert response.status_code == 204

    new = client.get('/api/orders/1/items')
    assert init.json != new.json
    assert any(resp['id'] == 3 for resp in new.json)


def test_delete_order(client, app):
    """
    Test delete order items
    """
    init = client.get('/api/orders/1/items')
    mock_item = dict(item_id=3)
    response = client.delete('/api/orders/1/items', json=mock_item)
    assert response.status_code == 204
    new = client.get('/api/orders/1/items')
    assert init.json != new.json and not any(resp['id'] == 3 for resp in new.json)

