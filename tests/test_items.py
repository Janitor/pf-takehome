def test_get_items(client, app):
    response = client.get('/api/items/')
    assert int(response.json['total']) > 0
    assert response.json['items']


def test_post_item(client, app):
    mock_item = dict(name="Shoelace")
    response = client.post('/api/items/', json=mock_item)
    assert response.status_code == 201
    response = client.get('/api/items/')
    assert any(resp['name'] == 'Shoelace' for resp in response.json['items'])


def test_get_item_id(client, app):
    response = client.get('/api/items/2')
    assert response.status_code == 200
    assert response.json['name'] == 'pizza'


def test_put_item(client, app):
    init = client.get('/api/items/4').json

    mock_item = dict(name="UnitTests")
    response = client.put('/api/items/4', json=mock_item)
    assert response.status_code == 204

    new = client.get('/api/items/4').json
    assert init['id'] == new['id']
    assert init['name'] != new['name'] and new['name'] == 'UnitTests'


def test_delete_item(client, app):
    response = client.delete('/api/items/4')
    assert response.status_code == 204

    response = client.get('/api/items/4')
    assert response.status_code == 404 and \
        'No results matching criteria.' in response.json['message']
