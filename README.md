# Project Franklin Software Challenge


# Getting Started
This project can be run with the provided Dockerfile, or from the command line. 
Once the project is hosted the Swagger documentation can be found at `localhost:5000/api`, each endpoint is located at `localhost:5000/api/<endpoint>`.

```bash
git clone git@gitlab.com:Janitor/pf-takehome.git
cd pf-takehome
```

## Prerequisites

 - Docker(-Compose) v18 - Acting as a virtualenv to ensure the code stability.

### Requirements.txt 

 - Flaskv1.0.2 - One of the lightweight web frameworks, easy to host and build upon.
 - Flask-RestPlusv0.12.1 - An integration for flask that encourages REST practices and allows documentation to be exposed through Swagger.
 - Flask-SQLAlchemyv2.3.2 - A powerful ORM. Allows for quick prototyping and can easily be switched to use a different database when this project needs to expand.
 - PyTestv4.3.0 - A popular Python testing framework

## Running & Testing
#### Running
```bash
docker-compose up -d --build 
```
#### Testing:
```bash
docker-compose exec web pytest
```

The project can also be run and tested locally:

```bash
FLASK_CONFIG=development
DATABASE_URI=sqlite:///franklin.db
pip install -r requirements.txt
pytest && python run.py
```

